module Hello12factors
  extend self

  def application_name
    ENV["HELLO12_NAME"]? || "Default name"
  end

  def database_url
    ENV["DATABASE_URL"]? || "localhost"
  end

  def redis_url
    ENV["REDIS_URL"]? || "localhost"
  end
end
