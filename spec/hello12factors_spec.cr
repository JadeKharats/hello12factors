require "./spec_helper"

describe Hello12factors do
  it "return default name" do
    Hello12factors.application_name.should eq("Default name")
  end

  it "return name in env variable" do
    ENV["HELLO12_NAME"] = "Pikachu"
    Hello12factors.application_name.should eq("Pikachu")
  end
end
